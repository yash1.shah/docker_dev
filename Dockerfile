
FROM node:7
WORKDIR /var/lib/jenkins/workspace/docker_dev
COPY . /var/lib/jenkins/workspace/docker_dev/
RUN npm install
CMD node index.js
EXPOSE 1337


